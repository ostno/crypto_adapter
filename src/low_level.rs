#[test]
fn create_hash() {
    let data_to_hash = b"some data";
    let digest = sodiumoxide::crypto::hash::hash(data_to_hash);
    let digest2 = sodiumoxide::crypto::hash::hash(data_to_hash);
    assert_eq!(digest, digest2);
}
