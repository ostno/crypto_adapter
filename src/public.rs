use sodiumoxide::crypto::box_;
use sodiumoxide::crypto::sealedbox;
use sodiumoxide::crypto::sign;
use std::str;

pub type BoxPublicKey = sodiumoxide::crypto::box_::PublicKey;
pub type BoxSecretKey = sodiumoxide::crypto::box_::SecretKey;
pub type BoxNonce = sodiumoxide::crypto::box_::Nonce;

pub struct Keys {
    pub public: BoxPublicKey,
    pub secret: BoxSecretKey,
}

pub fn generate_keys() -> Keys {
    let (pub_key, sec_key) = box_::gen_keypair();
    Keys {
        public: pub_key,
        secret: sec_key,
    }
}

pub fn generate_nonce() -> BoxNonce {
    box_::gen_nonce()
}

pub struct KeysRef<'a> {
    pub public: &'a BoxPublicKey,
    pub secret: &'a BoxSecretKey,
}

pub fn encrypt_message(message: &str, keys: KeysRef, nonce: &BoxNonce) -> Vec<u8> {
    let byte_string_message = message.as_bytes();
    let ciphertext = box_::seal(byte_string_message, nonce, keys.public, keys.secret);
    ciphertext
}

pub fn decrypt_message(message: &Vec<u8>, keys: KeysRef, nonce: &BoxNonce) -> String {
    let byte_message = box_::open(message, &nonce, keys.public, keys.secret).unwrap();
    let message = str::from_utf8(&byte_message).unwrap();
    message.to_string()
}

pub fn encrypt_message_anonymously(message: &str, key: &BoxPublicKey) -> Vec<u8> {
    let byte_string_message = message.as_bytes();
    sealedbox::seal(byte_string_message, key)
}

pub fn unseal(sealed_message: &Vec<u8>, keys: KeysRef) -> Result<String, ()> {
    match sealedbox::open(sealed_message, keys.public, keys.secret) {
        Ok(vec) => {
            let message = str::from_utf8(&vec).unwrap();
            Ok(message.to_string())
        }
        Err(e) => Err(e),
    }
}

pub type SignPublicKey = sodiumoxide::crypto::sign::PublicKey;
pub type SignSecretKey = sodiumoxide::crypto::sign::SecretKey;

pub struct SignKeys {
    pub secret: SignSecretKey,
    pub public: SignPublicKey,
}

pub fn generate_sign_keys() -> SignKeys {
    let (pk, sk) = sign::gen_keypair();
    SignKeys {
        secret: sk,
        public: pk,
    }
}

pub fn sign(data: &str, secret_key: &SignSecretKey) -> Vec<u8> {
    let byte_string_message = data.as_bytes();
    sign::sign(byte_string_message, secret_key)
}

pub fn verify(signed_data: &Vec<u8>, public_key: &SignPublicKey) -> Result<String, ()> {
    match sign::verify(signed_data, public_key) {
        Ok(vec) => {
            let message = str::from_utf8(&vec).unwrap();
            Ok(message.to_string())
        }
        Err(e) => Err(e),
    }
}

/// 
///Below are benchmark and test functions
/// 
pub fn encrypt_and_decrypt() {
    let sender_keys = generate_keys();
    let receiver_keys = generate_keys();
    let nonce = generate_nonce();
    let message = "A secret message";

    let keys_for_encryption = KeysRef {
        secret: &sender_keys.secret,
        public: &receiver_keys.public,
    };
    let encrypted_message = encrypt_message(&message, keys_for_encryption, &nonce);

    let keys_for_decryption = KeysRef {
        secret: &receiver_keys.secret,
        public: &sender_keys.public,
    };
    let decrypted_message = decrypt_message(&encrypted_message, keys_for_decryption, &nonce);

    assert_eq!(message, decrypted_message);
}

#[test]
fn encrypt_and_decrypt_test() {
    encrypt_and_decrypt();
}

pub fn sign_and_verify() {
    let keys = generate_sign_keys();
    let message = "Message to sign";
    let signed_message = sign(&message, &keys.secret);

    assert_eq!(message, verify(&signed_message, &keys.public).unwrap());
}

#[test]
fn sign_and_verify_test() {
    sign_and_verify();
}

pub fn anonymous_seal_and_unseal() {
    let keys_receiver = generate_keys();
    let message = "Message from a secret lover";
    let encrypted_message = encrypt_message_anonymously(&message, &keys_receiver.public);
    let keys_ref = KeysRef {
        secret: &keys_receiver.secret,
        public: &keys_receiver.public,
    };
    let decrypted_message = unseal(&encrypted_message, keys_ref).unwrap();

    assert_eq!(message, decrypted_message);
}

#[test]
pub fn anonymous_seal_and_unseal_test() {
    anonymous_seal_and_unseal();
}
