use criterion::{criterion_group, criterion_main, Criterion, BenchmarkId};

fn bench_fibs(c: &mut Criterion) {
    let mut group = c.benchmark_group("sodiumoxyde");

    let input = &0;
    group.bench_with_input(BenchmarkId::new("PUBLIC -> Encrypt-decrypt", input), input, 
        |b, _i| b.iter(|| crypto_adapter::public::encrypt_and_decrypt()));
    group.bench_with_input(BenchmarkId::new("PUBLIC -> Anonymous seal-unseal", input), input, 
        |b, _i| b.iter(|| crypto_adapter::public::anonymous_seal_and_unseal()));
    group.bench_with_input(BenchmarkId::new("PUBLIC -> Sign-verify", input), input, 
        |b, _i| b.iter(|| crypto_adapter::public::sign_and_verify()));
    group.bench_with_input(BenchmarkId::new("SECRET -> Encrypt-decrypt", input), input, 
        |b, _i| b.iter(|| crypto_adapter::secret::encrypt_and_decrypt()));
    group.bench_with_input(BenchmarkId::new("SECRET -> Stream message", input), input, 
        |b, _i| b.iter(|| crypto_adapter::secret::secret_stream_message()));

    group.finish();
}

criterion_group!(benches, bench_fibs);
criterion_main!(benches);