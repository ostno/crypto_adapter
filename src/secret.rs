use secretstream::{gen_key, Stream, Tag};
use sodiumoxide::crypto::secretbox;
use sodiumoxide::crypto::secretstream;
use std::str;

pub type SecretKey = secretbox::Key;
pub type SecretKeyStream = secretstream::xchacha20poly1305::Key;
pub type Nonce = secretbox::Nonce;
pub type Header = secretstream::xchacha20poly1305::Header;

pub fn generate_key() -> SecretKey {
    secretbox::gen_key()
}

pub fn generate_nonce() -> Nonce {
    secretbox::gen_nonce()
}

pub fn encrypt_message(message: &str, key: &SecretKey, nonce: &Nonce) -> Vec<u8> {
    let byte_string_message = message.as_bytes();
    let ciphertext = secretbox::seal(byte_string_message, nonce, key);
    ciphertext
}

pub fn decrypt_message(message: &Vec<u8>, key: &SecretKey, nonce: &Nonce) -> String {
    let byte_message = secretbox::open(message, nonce, key).unwrap();
    let message = str::from_utf8(&byte_message).unwrap();
    message.to_string()
}

pub struct StreamEncrypt<'a> {
    key: &'a SecretKeyStream,
    enc_stream: secretstream::xchacha20poly1305::Stream<secretstream::xchacha20poly1305::Push>,
    header: Header,
}

pub struct StreamDecrypt<'a> {
    key: &'a SecretKeyStream,
    dec_stream: secretstream::xchacha20poly1305::Stream<secretstream::xchacha20poly1305::Pull>,
}

// TODO : Tag are not implemented (should be MESSAGE then PUSH (n times) then FINAL)
impl<'a> StreamEncrypt<'a> {
    fn new(key: &'a SecretKeyStream) -> Self {
        let (enc_stream, header) = Stream::init_push(&key).unwrap();
        StreamEncrypt {
            key,
            enc_stream,
            header,
        }
    }
    fn push(&mut self, msg: &str) -> Vec<u8> {
        self.enc_stream
            .push(msg.as_bytes(), None, Tag::Message)
            .unwrap()
    }
}
impl<'a> StreamDecrypt<'a> {
    fn new(key: &'a SecretKeyStream, header: &'a Header) -> Self {
        StreamDecrypt {
            key,
            dec_stream: Stream::init_pull(&header, &key).unwrap(),
        }
    }
    fn pull(&mut self, msg: &Vec<u8>) -> String {
        let (decrypted, _tag) = &self.dec_stream.pull(msg, None).unwrap();
        let message = str::from_utf8(decrypted).unwrap();
        message.to_string()
    }
}

pub fn encrypt_and_decrypt() {
    let secret_key = generate_key();
    let nonce = generate_nonce();
    let message = "A secret message";

    let encrypted_message = encrypt_message(&message, &secret_key, &nonce);

    let decrypted_message = decrypt_message(&encrypted_message, &secret_key, &nonce);

    assert_eq!(message, decrypted_message);
}
#[test]
fn encrypt_and_decrypt_test() {
    encrypt_and_decrypt();
}

pub fn secret_stream_message() {
    let stream_key = gen_key();
    let mut encrypted_stream = StreamEncrypt::new(&stream_key);
    let mess1_enc = encrypted_stream.push("I");
    let mess2_enc = encrypted_stream.push("am");
    let mess3_enc = encrypted_stream.push("your father");

    let mut decrypted_stream = StreamDecrypt::new(&stream_key, &mut encrypted_stream.header);
    let mess1_dec = decrypted_stream.pull(&mess1_enc);
    let mess2_dec = decrypted_stream.pull(&mess2_enc);
    let mess3_dec = decrypted_stream.pull(&mess3_enc);

    assert_eq!(mess1_dec, "I");
    assert_eq!(mess2_dec, "am");
    assert_eq!(mess3_dec, "your father");
}

#[test]
fn secret_stream_message_test() {
    secret_stream_message();
}
